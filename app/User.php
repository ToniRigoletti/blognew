<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;
     protected $table = 'users';
    protected $primaryKey = 'user_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function roles(){
		
		return $this->belongsToMany('App\Role','role_user','user_id','role_id');
	}
    public static function getRole(){
        return DB::select("SELECT roles.name_role, users.user_id FROM roles INNER JOIN role_user on roles.role_id = role_user.role_id JOIN users on users.user_id = role_user.user_id ");
    }
    public static  function getUsers(){
        return DB::select("SELECT users.user_id, users.name, users.email, users.people_id FROM users");
    }
}
