<?php

namespace App;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
   	protected $table = 'comments';
    protected $primaryKey = 'comment_id';
    protected $fillable = [
        'title_comment', 'text_comment', 'text_comment' ,'commentator_name','status_id',
    ];
    public static function get_comment ($articl_id){
    	return DB::table('comments')->where('articl_id',$articl_id)->where('status_id',1)->get();
    }
    public  function save_comment(Request $req){

    		$this->text_comment = $req->input('Comment');
    		$this->commentator_name  =$req->input('Name');
    		$this->articl_id = $req->input('articl_id');
            $this->status_id = 2;


    	 return $this->save();

    }
    public static function get_comment_in_admin($articl_id){
        return DB::table('comments')->where('articl_id',$articl_id)->get();
    }

    public function save_edit(Request $req, $comment){
           
            $comment->status_id = $req->input('status_id');
            

        return $comment->save();
    }

}
