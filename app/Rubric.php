<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Rubric extends Model
{	protected $primaryKey = 'rubric_id';
    public static function get_all_rubric() {
    	return DB::table('rubrics')->get();
    }
    public static function get_name_rubric($rubric_id) {
    	return DB::table('rubrics')->where('rubric_id',$rubric_id)->get();
    }
    
}
