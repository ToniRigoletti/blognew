<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Articl extends Model
{	protected $primaryKey = 'articl_id';
	protected $table = "articles";
 protected $fillable = [
        'title',  'text' ,'rubric_id'
    ];
 
    public static function get_post_in_rubric($rubric_id){
    	return DB::table('articles')->where('rubric_id',$rubric_id)->paginate(1);

    }
    public static function get_all_post(){
    	return DB::table('articles')->paginate(1);
    }
    public static function get_post($articl_id){
    	return DB::select('SELECT * FROM articles INNER JOIN rubrics on articles.rubric_id = rubrics.rubric_id WHERE articles.articl_id ='.$articl_id);
    }
    public function post() {
    	return $this->belongsTo('App\Rubric','rubric_id'.'rubric_id');
    }
    public static function post_in_admin(){
    	return DB::select('SELECT * FROM articles INNER JOIN rubrics on articles.rubric_id = rubrics.rubric_id');
    }

    public function save_articl(Request $req) {


    		$this->title = $req->input('title');
    		$this->text  =	$req->input('text');
    		$this->rubric_id = $req->input('rubric_id');

    	return $this->save();
    }
    public function save_edit_articl(Request $req, $articl) {


    		$articl->title = $req->input('title');
    		$articl->text  =	$req->input('text');
    		$articl->rubric_id = $req->input('rubric_id');

    	return $articl->save();
    }
    public  static function delete_articl($articl_id){
        DB::delete("DELETE FROM articles WHERE articl_id = $articl_id");
    }
    //
}
