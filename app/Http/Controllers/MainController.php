<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articl;
use App\Rubric;
USE App\Comment;
class MainController extends Controller
{
    

    public function index(){
	    	$rubrics = Rubric::get_all_rubric();
	    	$articles = Articl::get_all_post();
	    	return view("home",compact('rubrics','articles')); 

       }
    public function show_post_in_rubric($rubric_id){
    	$rubrics = Rubric::get_all_rubric();
    	
    	$articles = Articl::get_post_in_rubric($rubric_id);
    	$rubric_id	*=1;
    	return view("rubric",compact('articles','rubrics','rubric_id'));

       }

    public function show_post($articl_id){
    	$comments = Comment::get_comment($articl_id);
    	$articl = Articl::get_post($articl_id);
    	return view('articl',compact('articl','comments'));
    }
    public function set_comment(Request $req){
    	$new_comment = new Comment();
    	$new_comment->save_comment($req);
    	return redirect('/');
    }

}
