<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Articl;
use App\Rubric;
use App\Comment;
use App\StatusComment;
use Auth;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\Role;
class AdminController extends Controller
{
    public function index(){
        
        if(Auth::user() !== null){
    	$articls = Articl::post_in_admin();
    	return view('admin.index',compact('articls'));
        }
            return redirect('');
    }
                   
    
    public function create(){
        $user = Auth::user();
        foreach ($user->roles as $role) {
            if($role->name_role === 'Admin'){
        	$rubrics = Rubric::get_all_rubric();
        	return view('admin.create',compact('rubrics'));
            }else return redirect('/');
       }

    }
    public function save_new_articl(Request $req) {
    	$new_articl = new Articl();
    	$new_articl->save_articl($req);

    	return redirect('http://blog-new.com/admin');
    }
    public function edit_articl($articl_id){
        $status = StatusComment::get_all_status();
    	$rubrics = Rubric::get_all_rubric();
    	$articl = Articl::get_post($articl_id);
    	$comments = Comment::get_comment_in_admin($articl_id);
    	return view('admin.edit',compact('articl','comments',"rubrics",'status'));
    }
    public function save_edit_articl(Request $req){
    	$articl = Articl::find($req->articl_id);
    	$articl->save_edit_articl($req, $articl);
    	return redirect('http://blog-new.com/admin');
    }
    public function save_edit_comment(Request $req){
        $comment = Comment::find($req->input('comment_id'));
        $comment->save_edit($req, $comment);
        return redirect("http://blog-new.com/admin/articl/edit/".$comment->articl_id); 
    }
    public function delete_articl($id){
       
        Articl::delete_articl($id);
        $articls = Articl::post_in_admin();
        return redirect("http://blog-new.com/admin");
    }
}
