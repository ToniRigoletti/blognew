<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class StatusComment extends Model
{
    
    public static function get_all_status(){
    	return DB::table('status_comment')->get();
    }
}
