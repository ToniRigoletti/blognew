 <nav class="navbar navbar-expand-md  navbar-dark bg-dark ">
        
    <a class="navbar-brand"  href="/" >Блог </a>
     <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
         <li class="nav-item active">
            <a class="nav-link" href="http://blog-new.com/">Главная <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
         <a class="nav-link" href="/contact">Контакты</a>
        </li>
        <li class="nav-item">
         <a class="nav-link" href="/aboutUs">О нас</a>
        </li>
        @auth
        @if(Auth::user())
            @foreach(Auth::user()->roles as $role)
                @if($role->name_role === 'Admin' )        
                    <li class="nav-item"><a href="/admin" class="nav-link">Admin</a></li>
                @endif
            @endforeach
        @endif 
        
        </ul>
    </div>
    
    @if (Route::has('login'))
         <div class="top-right links alert alert-dark ">
                
                 <a href="/logout">Logout</a>
                 <a  class="nav-link " href="#">
                                    {{ Auth::user()->name }} 
                                </a>
            
              
                    @else
                        
                
              

             @endauth
         </div>
     @endif
     
     
    
 </nav>    
 <br />
 