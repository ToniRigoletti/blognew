@extends('index')


@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
   
        
    
        <h2>Добавление новой статьи </h2>
        <div class="container">
            {{ Form::open(['url'=>'/admin/articl/create' ,'enctype'=>'multipart/form-data']) }}
    <div class="form-row">
        {{Form::label('title','Название статьи:')}}
        {{Form::text('title',null,['class'=>'form-control'])}}
    </div>
        <div class="form-row">
        {{Form::label('text','Текст:')}}
        {{Form::textarea('text',null,['class'=>'form-control'])}}
        </div>
        <div class="form-row">
            <select name="rubric_id" >
                @foreach($rubrics as $rub)
        
                <option name="rubric_id" value="{{$rub->rubric_id}}">{{$rub->name_rubric}}</option>
                @endforeach
            </select>
        </div>

                    <div class="form-group col-md-2">
                {{Form::submit('Добавить статью')}}
            </div>
    {{ Form::close() }}
        </div>

@endsection