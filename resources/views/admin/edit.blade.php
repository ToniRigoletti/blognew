@extends('index')


@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
   
        
    
        <h2> Редактор статьи </h2>
        <div class="container">
            {{ Form::open(['url'=>'/admin/articl/edit/'.$articl[0]->articl_id ,'enctype'=>'multipart/form-data']) }}
    <div class="form-row">
        {{Form::label('title','Название статьи:')}}
        {{Form::text('title',$articl[0]->title,['class'=>'form-control'])}}
    </div>
        <div class="form-row">
        {{Form::label('text','Текст:')}}
        {{Form::textarea('text',$articl[0]->text,['class'=>'form-control'])}}
        </div>
        <input type="hidden" name="articl_id" value="{{$articl[0]->articl_id}}">
        <div class="form-row">
            <select name="rubric_id" >
                @foreach($rubrics as $rub)
        
                <option name="rubric_id" value="{{$rub->rubric_id}}">{{$rub->name_rubric}}</option>
                @endforeach
            </select>
        </div>

                    <div class="form-group col-md-2">
                {{Form::submit('Сохранить изменения')}}
            </div>
    {{ Form::close() }}
        </div>
    <div class="container">
    <p>Комментарии к статье</p>
   
           
            @foreach ($comments as $comment)
               
               @if($comment->status_id === 2)
                <div class="alert alert-warning">
                @else <div class="alert alert-info">
                @endif
                    {{$comment->commentator_name}}
                    <p>
                        {{$comment->text_comment}}
                         <div class="container">
            
               
                 
                      {{ Form::open(['url'=>'/admin/articl/edit/'.$articl[0]->articl_id.'/comment' ,'enctype'=>'multipart/form-data']) }}
                     
                      
                            <div class="form-row">
                         <select name="status_id">
                            @foreach($status as $stat)
        
                             <option name="status_id" value="{{$stat->status_id}}">{{$stat->status}}</option>
                        @endforeach
                        </select>
                         </div>
                        
                        {{Form::hidden('comment_id',$comment->comment_id) }}                        
                      <div class="form-group col-md-2">
                            {{Form::submit('Изменить статус комментария')}}
                      </div>
                      {{ Form::close() }}
               
                    </div>
               
             </div>
        </div>
                    </p>
                
               
            @endforeach 
        </div>


@endsection