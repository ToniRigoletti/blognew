@extends('index')

@section('content')
	<table class="table">
  		<thead>
		<tr>
			<th>ID</th>
			<th>Название статьи</th>
			<th>Рубрика</th>
			<th></th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		
			@foreach($articls as $art)
				<tr>
					<td>{{$art->articl_id}}</td>
					<td>{{$art->title}}</td>
					<td>{{$art->name_rubric}}</td>
					<td><a href="/admin/articl/edit/{{$art->articl_id}}">Изменить</a></td>
					<td><div  class="btn btn-warning delete_atricl"  id="{{$art->articl_id}}">Удалить</div></td>
				</tr>
			@endforeach
		
		<tr>
			<td></td> <td></td> <td></td> <td></td><td><a href="admin/articl/create" class="btn btn-primary my-2">Добавить новую стаью</a></td>
		</tr>
		</tbody>
  	</table>

@endsection