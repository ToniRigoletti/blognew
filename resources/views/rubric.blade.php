@extends('index')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="btn-group center" role="group" aria-label="Basic example">
                          @foreach($rubrics as $rub)
                          @if($rubric_id === $rub->rubric_id )
		        <a  href="/rubric/{{$rub->rubric_id}}" class="btn btn-secondary list-group-item-info">{{$rub->name_rubric}}</a>
		        @else <a  href="/rubric/{{$rub->rubric_id}}" class="btn btn-secondary">{{$rub->name_rubric}}</a>
				@endif
                          
                         @endforeach
                    </div>
                </div>
    
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @foreach ($articles as $art)

        
                <h2>
                    <a href="/articl/{{$art->articl_id}}">{{$art->title}}</a>
                </h2>
                <p>
                    {{$art->text}}
                </p>
            @endforeach
        </div>
        {{$articles->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection