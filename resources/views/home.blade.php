@extends('index')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="btn-group center" role="group" aria-label="Basic example">
                          @foreach($rubrics as $rub)
                          <a  href="/rubric/{{$rub->rubric_id}}" class="btn btn-secondary">{{$rub->name_rubric}}</a>
                         @endforeach
                    </div>
                </div>
    
                <div class="card-body">
                

                    @foreach ($articles as $art)

        
                <h2>
                    <a href="/articl/{{$art->articl_id}}">{{$art->title}}</a>
                </h2>
                <p>
                    {{$art->text}}
                </p>
            @endforeach
        </div>
        {{$articles->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
