<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Pricing example for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('css/pricing.css') }}" rel="stylesheet">
  </head>

  <body >

<a href="/login">Login</a>
                        <a href="/register">Register</a>
@include('layouts.head')
  
  <main role="main ">

   <div class="container-fluid ">

      

     @yield('content')

  
    
    @include('layouts.back')
    </div>
    </main>

        @include('layouts.footer')
    
 
        
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="{{ URL::asset('js/jquery-3.3.1.js') }}" language="JavaScript" type="text/javascript"></script>
  <script src="{{ URL::asset('js/app.js') }}" ></script>
    <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
   <script src="{{ URL::asset('js/bootstrap.js') }}" ></script>



    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  </body>
</html>
