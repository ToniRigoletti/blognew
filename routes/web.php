<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;


Route::get('/',"MainController@index");
Route::get('/rubric/{rubric_id}','MainController@show_post_in_rubric');
Route::get('/articl/{articl_id}','MainController@show_post');
Route::post('/articl/{articl_id}','MainController@set_comment');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

	Route::get('/admin',"AdminController@index");
	Route::get('/admin/articl/create','AdminController@create');
	Route::post('/admin/articl/create','AdminController@save_new_articl');
	Route::get('/admin/articl/edit/{articl_id}',"AdminController@edit_articl");
	Route::post('/admin/articl/edit/{articl_id}',"AdminController@save_edit_articl");
	Route::post('/admin/articl/edit/{articl_id}/comment',"AdminController@save_edit_comment");
   	Route::get('/delete/{id}',"AdminController@delete_articl");


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
